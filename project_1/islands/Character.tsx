import { useEffect, useState } from "preact/hooks";

export default function CharacterList() {
  const [characters, setCharacters] = useState<ICharacter[]>([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function fetchCharacters() {
      try {
        const fetchedData = await fetch(
          "https://rickandmortyapi.com/api/character",
        );
        if (fetchedData.statusText === "OK") {
          const { results: fetchedCharacters } = await fetchedData.json();
          setCharacters(fetchedCharacters);
        } else {
          throw new Error(
            `fetchedData status not OK. Returned ${fetchedData.status}`,
          );
        }
      } catch (fetchCharactersError) {
        throw new Error(
          `couldn't fetch character, returns ${fetchCharactersError}`,
        );
      } finally {
        setIsLoading(false);
      }
    }
    fetchCharacters();
  }, [characters.length]);

  return (
    <article>
      <h2>Characters</h2>
      <ul>
        {isLoading
          ? (
            <li>
              <p>Loading...</p>
            </li>
          )
          : (characters as ICharacter[]).map((character) => (
            <li key={character.id}>
              <Character
                key={character.id}
                name={character.name}
                image={character.image}
                origin={character.origin}
              />
            </li>
          ))}
      </ul>
    </article>
  );
}

interface ICharacter {
  id: number;
  name: string;
  origin: Record<string, string>;
  image: string;
}

function Character(character: ICharacter) {
  return (
    <section className="card">
      <h3>{character.name}</h3>
      <img
        src={character.image}
        alt={`image of ${character.name}`}
        width="300"
      />
      <p>{`Origin: ${character.origin?.name ?? "unknown"}`}</p>
    </section>
  );
}
