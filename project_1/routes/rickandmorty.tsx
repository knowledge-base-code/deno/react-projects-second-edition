import { asset, Head } from "$fresh/runtime.ts";
import CharacterList from "../islands/Character.tsx";

export default function RickAndMorty() {
  return (
    <>
      <Head>
        <title>Rick and Morty</title>
        <link rel="stylesheet" href={asset("/index.css")} />
      </Head>
      <main>
        <header>Rick and Morty</header>
        <CharacterList />
      </main>
    </>
  );
}
